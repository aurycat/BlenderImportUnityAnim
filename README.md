# Blender Import Unity Anim

This package contains a Blender plugin to import Unity animations (.anim),
as well as a Unity script to convert Humanoid animations and ShaderMotion
recordings into the format required by the Blender plugin.


## Download

### [Download the latest unitypackage here.](https://gitlab.com/aurycat/BlenderImportUnityAnim/-/raw/releases/BlenderImportUnityAnim-v1.0.unitypackage)

Or view all releases [here](https://gitlab.com/aurycat/BlenderImportUnityAnim/-/releases).


## Importing a ShaderMotion recording into Blender

If you have a ShaderMotion video recording and want to import it onto an
armature in Blender, follow these steps:

1. Download and import [the latest ShaderMotion package](https://gitlab.com/lox9973/ShaderMotion/-/releases).
   It comes as a .zip, not .unitypackage. Extract it and import it into the project.
2. Import the ShaderMotion video into your project's assets.
3. Import this package into your project.
4. Open the scene named 'AnimConversionScene'.
5. Import your avatar model into the scene; the avatar should be configured as
   Humanoid. (Adjust that in the model import settings. If the avatar is already
   setup for use in VRChat, it should already be configured correctly.)<br>
   **IMPORTANT: The armature of this avatar must be identical to the armature in
   Blender that the animation will be imported onto. If you have multiple avatars
   in Blender that you want to import the animation onto, you must do this conversion
   process for each avatar separately.**
6. In the inspector for the the Recorder object, set the Animator property to your avatar's animator.
7. Change Source Mode dropdown to 'Shader Motion Video', select your video file,
   adjust any other settings as you see fit, and then click 'Start Recording'.
8. Once recording is complete, a new transform animation will be produced, defaulting
   to Assets/Recording.anim, which can be imported into Blender. Continue with
   the ["Importing a transform animation into Blender" section](#importing-a-transform-animation-into-blender).


## Importing a Humanoid animation into Blender

If you have an .anim file which contains Humanoid animation curves (see the
["What is a Humanoid animation?" section](#overview-of-terms---what-is-a-humanoid-animation) below) and want to import it onto an
armature in Blender, follow these steps:

1. Follow steps 3-6 in "Importing a ShaderMotion recording into Blender"
2. Change Source Mode dropdown to 'Animation Clip', choose your source animation
   clip, adjust any other settings as you see fit, and then click 'Start Recording'.
3. Once recording is complete, a new transform animation will be produced, defaulting
   to Assets/Recording.anim, which can be imported into Blender. Continue with
   the ["Importing a transform animation into Blender" section](#importing-a-transform-animation-into-blender).


## Importing a transform animation into Blender

If you have an .anim file which only contains transform animation curves
(i.e. directly animating the position/rotation/scale of avatar bones) and
want to import it onto an armature in Blender, follow these steps:

Install the BlenderImportUnityAnim.py plugin:
1. In Blender, go to Edit > Preferences and go to the "Add-ons" pane
2. Click "Install..."
3. Navigate to [BlenderPlugin/BlenderImportUnityAnim.py](BlenderPlugin/BlenderImportUnityAnim.py) 
4. Enable the add-on by checking the toggle box next to "Import-Export: Import Unity Anim"

Import the anim:
1. Select the armature in your scene to import the animation onto.<br>
   **IMPORTANT: The armature that the animation is imported onto must be identical
   to the armature that the animation was created for!!!**
2. Go to File > Import > Unity Armature Anim (.anim).
3. Adjust the import settings (right-side of import window) as necessary.<br>
   **IMPORTANT: Match the scale import settings to the corresponding model import
   settings in Unity.**
4. Navigate to the .anim file and click Import.


## Overview of terms - What is a Humanoid animation?

Unity animations come in two forms, both stored in .anim files. The first is
["Humanoid" animations](https://blog.unity.com/engine-platform/mecanim-humanoids),
which only work on avatars configured as Humanoid in the model's import settings.
They animate "muscles", and are usually created via Unity's automatic conversion
when importing a 3D model that has animations. Humanoid animations look like this:

![Example of a humanoid animation](Images/HumanoidAnimation.png)

The other form is non-Humanoid animations, or as I call them in this project,
"transform animations". These directly animate individual GameObject transforms,
e.g. position and rotation. (These animations can also modify other things such as
material properties, but that is not important here.) Transform animations look
like this:

![Example of a non-humanoid animation](Images/NonhumanoidAnimation.png)

Humanoid animations are useful for game development in Unity because they allow
[retargeting](https://docs.unity3d.com/Manual/Retargeting.html), but it is a
proprietary format which would be difficult to create a Blender import plugin for.
It is possible for a single .anim to contain both Humanoid and non-Humanoid
animation curves, though that is not common.

**The Import Unity Anim plugin for Blender will ignore all Humanoid animation
curves in an .anim file. Therefore, if you have a Humanoid animation and want
to import it into Blender, it first needs to be converted using the included
Unity script.**


## Overview of terms - What is ShaderMotion?

[ShaderMotion](https://gitlab.com/lox9973/ShaderMotion/) is "a motion-to-video
codec for Unity humanoid avatars" created by lox9973. It allows capturing the
motion of a Humanoid avatar, recording or streaming it via video, and then
replaying the motion on another avatar. It is essentially mocap encoded in a
video. ShaderMotion makes use of Unity's Humanoid system, and so allows for
retargeting: the recorded animation can be played back on any avatar, not just
the avatar that was recorded.

ShaderMotion is very useful for recording mocap within a game like VRChat,
however there is no simple way to take that recording and import it into
Blender. The scripts in this package make it possible to convert a ShaderMotion
video into a non-Humanoid animation which can then be imported onto an armature
in Blender.

To create a ShaderMotion recording, the easiest way is to use the MOCAP Studio
world in VRChat:<br>
https://vrchat.com/home/world/wrld_859d580d-ff9c-4545-84ba-682440f64857

## Animation conversion method

Since conversion from Humanoid animation to bone transform animation is difficult,
this script takes the easy way out by just having Unity play the Humanoid animation
and then recording the final bone transforms it generates.

There already exist other scripts which can record property changes in Unity playmode
(for example, [this one I made](https://gitlab.com/aurycat/UnityEditorScripts/-/blob/main/LivePropertyRecorder.cs)), which
and could theoretically do the same job as this conversion script. However, those
recording scripts record at a fixed framerate ignoring the source animation framerate,
meaning the conversion is lossy.

This script will record in lock-step with the Humanoid animation playback, resulting
in essentially lossless conversion. Additionally, it has extra stuff to make sure the
recording and playback start and stop at the same time, which prevents any excess
garbage at the beginning or end of the recording.

Similarly, when converting a ShaderMotion video, the resulting animation will have
exactly one frame per original frame of video. If you want a lower framerate output
to save space (the resulting animations can be very large), you can first reduce the
framerate of the video using a video editor tool.

## Etc

This package and Blender plugin were made by aurycat. MIT license.

ShaderMotion was made by lox9973 who has no relation to this project.
