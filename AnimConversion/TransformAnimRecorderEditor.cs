﻿#if UNITY_EDITOR
using System;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using UnityEditor.Animations;

namespace aurycat {

[CustomEditor(typeof(TransformAnimRecorder))]
public class TransformAnimRecorderEditor : Editor
{
    SerializedProperty sourceModeProp;
    SerializedProperty sourceAnimClipProp;
    SerializedProperty sourceAnimControllerProp;
    SerializedProperty sourceVideoProp;
    SerializedProperty footIKProp;
    SerializedProperty animatorProp;
    SerializedProperty armatureRootProp;
    SerializedProperty realtimePlaybackAndRecordingProp;
    SerializedProperty realtimeRecordingFPSProp;
    SerializedProperty startDelaySecondsProp;
    SerializedProperty outputFileNameProp;
#if UNITY_2019_4_OR_NEWER
    SerializedProperty keyframeReductionProp;
#endif
    SerializedProperty maximizeGameViewProp;

    void OnEnable()
    {
        sourceModeProp = serializedObject.FindProperty("sourceMode");
        sourceAnimClipProp = serializedObject.FindProperty("sourceAnimClip");
        sourceAnimControllerProp = serializedObject.FindProperty("sourceAnimController");
        sourceVideoProp = serializedObject.FindProperty("sourceVideo");
        footIKProp = serializedObject.FindProperty("footIK");
        animatorProp = serializedObject.FindProperty("animator");
        armatureRootProp = serializedObject.FindProperty("armatureRoot");
        realtimePlaybackAndRecordingProp = serializedObject.FindProperty("realtimePlaybackAndRecording");
        realtimeRecordingFPSProp = serializedObject.FindProperty("realtimeRecordingFPS");
        startDelaySecondsProp = serializedObject.FindProperty("startDelaySeconds");
        outputFileNameProp = serializedObject.FindProperty("outputFileName");
        #if UNITY_2019_4_OR_NEWER
        keyframeReductionProp = serializedObject.FindProperty("keyframeReduction");
        #endif
        maximizeGameViewProp = serializedObject.FindProperty("maximizeGameView");
    }

    public override void OnInspectorGUI()
    {
        TransformAnimRecorder recorder = (TransformAnimRecorder)target;
        TransformAnimRecorder.SourceAnimationMode sourceMode = (TransformAnimRecorder.SourceAnimationMode)sourceModeProp.enumValueIndex;
        AnimationClip sourceAnimClip = sourceAnimClipProp.objectReferenceValue as AnimationClip;
        AnimatorController sourceAnimController = sourceAnimControllerProp.objectReferenceValue as AnimatorController;
        VideoClip sourceVideo = sourceVideoProp.objectReferenceValue as VideoClip;
        bool footIK = footIKProp.boolValue;
        Animator animator = animatorProp.objectReferenceValue as Animator;
        GameObject armatureRoot = armatureRootProp.objectReferenceValue as GameObject;
        bool realtimePlaybackAndRecording = realtimePlaybackAndRecordingProp.boolValue;
        string outputFileName = outputFileNameProp.stringValue.Trim();
        bool maximizeGameView = maximizeGameViewProp.boolValue;


        /*
         * Determine if the configuration is valid and the 'Start Recording'
         * button should be enabled. Further down, error help boxes will be
         * displayed for individual issues.
         */

        bool ok = true;
        bool badArmatureRoot = false;
        bool badOutputFileNameDir = false;
        bool emptyFileName = false;
        bool shaderMotionMissing = false;
        string outputDirName = "";

        if (animator == null) {
            ok = false;
        }
        else if (animator.GetComponentInChildren<SkinnedMeshRenderer>() == null) {
            ok = false;
        }

        if (armatureRoot != null && animator != null) {
            if (!armatureRoot.transform.IsChildOf(animator.transform)) {
                ok = false;
                badArmatureRoot = true;
            }
        }

        if (sourceMode == TransformAnimRecorder.SourceAnimationMode.AnimationClip) {
            if (sourceAnimClip == null) {
                ok = false;
            }
        }
        else if (sourceMode == TransformAnimRecorder.SourceAnimationMode.AnimatorController) {
            if (sourceAnimController == null) {
                ok = false;
            }
        }
        else if (sourceMode == TransformAnimRecorder.SourceAnimationMode.ShaderMotionVideo) {
            if (Type.GetType("ShaderMotion.MotionPlayer, ShaderMotion") == null) {
                ok = false;
                shaderMotionMissing = true;
            }
            else if (sourceVideo == null) {
                ok = false;
            }
        }

        if (outputFileName == "") {
            emptyFileName = true;
        }
        else {
            int slashPosition = outputFileName.LastIndexOf('/');
            if (slashPosition > -1) {
                outputDirName = outputFileName.Substring(0, slashPosition);
                if (!AssetDatabase.IsValidFolder("Assets/" + outputDirName)) {
                    ok = false;
                    badOutputFileNameDir = true;
                }
            }
        }


        /*
         * Draw the 'Start Recording'/'Stop Recording' button.
         */

        serializedObject.Update();

        GUILayout.Space(10);
        GUILayout.BeginVertical("box");
        GUILayout.Space(5);
        using (new EditorGUI.DisabledScope(!ok)) {
            if (EditorApplication.isPlaying) {
                if (recorder.enabled) { // yes recording
                    if (GUILayout.Button("Stop Recording")) {
                        recorder.Stop();
                    }
                }
                else { // not recording
                    if (GUILayout.Button("Start Recording")) {
                        recorder.enabled = true;
                    }
                }
            }
            else { // not in playmode
                if (GUILayout.Button("Start Recording")) {
                    recorder.enteredPlaymodeViaStartRecordingButton = true;
                    EditorApplication.EnterPlaymode();
                }
            }
        }
        GUILayout.Space(5);
        GUILayout.EndVertical();

        if (!EditorApplication.isPlayingOrWillChangePlaymode) {
            recorder.enteredPlaymodeViaStartRecordingButton = false;
        }


        /*
         * Section 'Animation Source'
         */

        EditorGUILayout.PropertyField(sourceModeProp);

        if (sourceMode == TransformAnimRecorder.SourceAnimationMode.AnimationClip) {
            EditorGUILayout.PropertyField(sourceAnimClipProp);
            if (sourceAnimClip == null) {
                //EditorGUILayout.HelpBox("If no Animation Clip is provided, the existing Animator on the avatar will play until you press 'Stop Recording'.", MessageType.Warning);
                EditorGUILayout.HelpBox("An animation clip is required. It should probably be a Humanoid (Mecanim) animation.", MessageType.Error);
            }
            EditorGUILayout.PropertyField(footIKProp);
        }
        else if (sourceMode == TransformAnimRecorder.SourceAnimationMode.AnimatorController) {
            EditorGUILayout.PropertyField(sourceAnimControllerProp);
            if (sourceAnimController == null) {
                EditorGUILayout.HelpBox("An animation controller is required.", MessageType.Error);
            }
            EditorGUILayout.HelpBox(
"The animator controller needs an empty state on the Base layer named STOP_RECORDING. " +
"Recording will end as soon as the animator enters that state.", MessageType.Info);
        }
        else if (sourceMode == TransformAnimRecorder.SourceAnimationMode.ShaderMotionVideo) {
            if (shaderMotionMissing) {
                EditorGUILayout.HelpBox(
"ShaderMotion is not installed, or the MotionPlayer component is missing. Please " +
"install ShaderMotion from https://gitlab.com/lox9973/ShaderMotion/-/releases .\n\n" +
"Download the 'Source code (zip)' from the latest release, then extract the zip " +
"file into your Unity project at Assets/ShaderMotion.", MessageType.Error);
                GUIStyle rich = new GUIStyle(GUI.skin.button);
                rich.richText = true;
                GUILayout.Space(10);
                if (GUILayout.Button("<b>Click here to open the ShaderMotion download page</b>", rich)) {
                    Application.OpenURL("https://gitlab.com/lox9973/ShaderMotion/-/releases");
                }
                GUILayout.Space(20);
            }
            else {
                EditorGUILayout.PropertyField(sourceVideoProp);
                if (sourceVideo == null) {
                    EditorGUILayout.HelpBox("A video clip is required. It must be a ShaderMotion video recording.", MessageType.Error);
                }
            }
        }


        /*
         * Section 'Avatar'
         */

        EditorGUILayout.PropertyField(animatorProp);
        if (animator == null) {
            EditorGUILayout.HelpBox("'Animator' must be a reference to the Animator component of the avatar to record.", MessageType.Error);
        }
        else if (animator.GetComponentInChildren<SkinnedMeshRenderer>() == null) {
            EditorGUILayout.HelpBox("No SkinnedMeshRenderer found on the avatar.", MessageType.Error);
        }

        EditorGUILayout.PropertyField(armatureRootProp,
            new GUIContent("Armature Root (Optional)"));
        if (badArmatureRoot) {
            EditorGUILayout.HelpBox(
"'Armature Root' must be a child object of the Animator. If your avatar's armature root is named " +
"'Armature' or 'Root', this field can be left blank as it will automatically be filled in.",
                MessageType.Error);
        }


        /*
         * Section 'Realtime Mode'
         */

        GUILayout.Space(10);
        EditorGUILayout.LabelField("Realtime Mode", EditorStyles.boldLabel);
        if (sourceMode == TransformAnimRecorder.SourceAnimationMode.AnimatorController) {
            using (new EditorGUI.DisabledScope(true)) {
                EditorGUILayout.Toggle("Realtime Playback And Recording", true);
            }
        }
        else {
            EditorGUILayout.PropertyField(realtimePlaybackAndRecordingProp);
        }

        if (realtimePlaybackAndRecording || sourceMode == TransformAnimRecorder.SourceAnimationMode.AnimatorController) {
            EditorGUILayout.PropertyField(realtimeRecordingFPSProp);
            EditorGUILayout.PropertyField(startDelaySecondsProp);
            GUILayout.Space(10);
            EditorGUILayout.HelpBox(
"When Realtime Playback And Recording is off, the animation source is played frame by frame, " +
"and each frame is captured exactly into the output anim, meaning the output is essentially " +
"lossless. However, the playback speed is not realtime; it will probably be faster than realtime, " +
"but may be slower depending on computer performance.\n\n" +
"With Realtime Playback And Recording enabled, the animation source is played in realtime and " +
"the recording happens at the chosen framerate. This means that the recording may be somewhat " +
"lossy, however it allows you to record things like physics simulations into the output animation. " +
"For example, if your avatar has VRC PhysBones, you can use Realtime Playback And Recording to " +
"bake the PhysBone physics into the output animation.\n\n"+
"Use the start delay time to let the physics settle into their idle position on the first " +
"frame of the animation before starting recording.",
                MessageType.Info);
            GUILayout.Space(10);
        }
        else {
            EditorGUILayout.HelpBox(
"Use Realtime Playback And Recording if you want to bake VRC PhysBone or DynamicBone physics into " +
"the animation. Enable to see more info.",
                MessageType.Info);
        }


        /*
         * Section 'Anim Output'
         */

        EditorGUILayout.PropertyField(outputFileNameProp);
        if (emptyFileName) {
            EditorGUILayout.HelpBox($"Default output is 'Recording.anim'", MessageType.Info);
        }
        else if (badOutputFileNameDir) {
            EditorGUILayout.HelpBox($"The directory '{outputDirName}' does not exist.", MessageType.Error);
        }

        #if UNITY_2019_4_OR_NEWER
        EditorGUILayout.PropertyField(keyframeReductionProp);
        #endif


        /*
         * Section 'Etc'
         */

        EditorGUILayout.PropertyField(maximizeGameViewProp);
        if (!maximizeGameView) {
            EditorGUILayout.HelpBox(
"It is best to leave this setting enabled. Maximizing the game view reduces the " +
"amount of other Editor rendering tasks that need to be run each frame, especially " +
"the Scene or Animator windows. This can dramatically speed up frame times and " +
"therefore make animation playback faster.\n\n" +
"If you disable this, make sure to hide the Scene and Animator windows during " +
"recording.", MessageType.Warning);
        }

        serializedObject.ApplyModifiedProperties();
    }
}

}
#endif