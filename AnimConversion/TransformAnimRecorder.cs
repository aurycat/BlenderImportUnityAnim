// Records armature motion from a source ShaderMotion video, Humanoid
// animation, or series of animations in an animator controller.
//
// This script is like ShaderMotion's existing AnimRecorder script.
// The difference is that this records in lock-step with video playback,
// meaning no data is lost or "generated" due to mismatched framerates,
// and that this exports as a bone-transform anim instead of Humanoid.
//
// USAGE: See attached README
//
// ------------------------------------------------------------------------------
//
// AUTHOR: aurycat
// LICENSE: MIT
// HISTORY:
//  1.0 (2023-07-07)


// If the ShaderMotion package is installed, this can be uncommented to
// use the MotionPlayer type directly instead of lookup-by-name. This
// should be left commented unless there is some issue.
//#define SHADER_MOTION_INSTALLED


#if UNITY_EDITOR
using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using UnityEditor.Animations;
#if SHADER_MOTION_INSTALLED
using ShaderMotion;
#endif

namespace aurycat {

public class TransformAnimRecorder : MonoBehaviour  {

    public enum SourceAnimationMode {
        AnimationClip,
        AnimatorController,
        ShaderMotionVideo,
    }


    /*
     * Inspector properties
     * (See TransformAnimRecorderEditor.cs)
     */

    [Header("Animation Source")]
    public SourceAnimationMode sourceMode;
    public AnimationClip sourceAnimClip;
    public AnimatorController sourceAnimController;
    public VideoClip sourceVideo;
    public bool footIK = true; // For sourceMode=AnimationClip only

    [Header("Avatar")]
    [Tooltip("The Animator on the avatar. Will be copied from the MotionPlayer component if null.")]
    public Animator animator;
    [Tooltip("The root of the avatar's armature. Must be a child object of the Animator object. Will be autodetected if null.")]
    public GameObject armatureRoot;

    //[Header("Realtime Mode")] // Header added by the Editor code
    public bool realtimePlaybackAndRecording = false;
    [Range(1,120)]
    public int realtimeRecordingFPS = 60;
    [Range(0,15)]
    public int startDelaySeconds = 1;

    [Header("Anim Output")]
    [Tooltip("Filename can include / to specify a path. Existing files will NOT be overwritten; the name will get a number appended to make it unique.")]
    public string outputFileName = "Recording.anim";
#if UNITY_2019_4_OR_NEWER
    [Range(0,100)]
    public float keyframeReduction = 0.5f;
#endif

    [Header("Etc")]
    public bool maximizeGameView = true;

    /*
     * Private variables
     */

    // For recording property changes each frame and saving them to an
    // .anim file. Used by all source modes.
    private GameObjectRecorder recorder;

    // Reference to the VideoPlayer component in the scene.
    // Used by sourceMode=ShaderMotionVideo.
    private VideoPlayer videoPlayer;

    // Reference to the MotionPlayerPrefab instance in the scene.
    // Used by sourceMode=ShaderMotionVideo.
    private GameObject motionPlayerInstance;

    // Used to count frames and create delays between frame captures.
    // Used by all source modes.
    private int frameState;

    // The frame rate of the final animation clip.
    // For sourceMode=AnimationClip, this value is copied from the input clip.
    // For sourceMode=AnimatorController, 'Realtime Playback And Recording' mode
    //  is required, so this value comes from the given recording framerate.
    // For sourceMode=ShaderMotionVideo, this value is the frame rate of the
    //  video clip.
    private float frameRate;

    // Initially true; set false once the first source frame has been captured.
    // Until this is set false, the GameObjectRecorder has no data and so te
    // .anim cannot be written. Used by all source modes.
    private bool firstFrame;

    // Indicates when the last frame of the source has been reached and recording
    // should end. Used by all source modes.
    // For sourceMode=AnimatorController, this is set once the animator's base
    // layer enters a state named STOP_RECORDING.
    private bool sourceCompleted;

    // Set true once an animation clip has been generated. Prevents saving the
    // clip twice in some cases.
    private bool saved;

    // Set true once the source is ready for capture.
    // For sourceMode=AnimationClip or AnimatorController, this var becomes true
    //  a fixed number of game-frames after this component has been enabled. The
    //  delay is to ensure the animator is presenting the first frame.
    // For sourceMode=ShaderMotionVideo, this var becomes true once the
    //  OnVideoPrepared handler has been called.
    private bool prepared;

    // Set to Time.time at the moment 'prepared' is set true.
    // Used by all source modes when 'Realtime Playback And Recording' is enabled
    // to implement the adjustable initial delay.
    private float preparedTime;

    // See TakeSnapshotRealtime for explanation
    private float lastRealtimeSnapshot;

    // Used by the editor when 'Start Recording' is pressed.
    [HideInInspector]
    public bool enteredPlaymodeViaStartRecordingButton;

    // Whether the Game View was maximized during OnEnable.
    private bool didMaximizeGameView;


    void Awake()
    {
        if (enteredPlaymodeViaStartRecordingButton) {
            enabled = true;
        }
    }

    void OnValidate()
    {
        // Don't allow the script to be enabled in edit mode. Prevents the
        // script from starting unintentionally if the user enters edit
        // mode, especially if there are multiple TransformAnimRecorders
        // in one scene.
        if (!EditorApplication.isPlayingOrWillChangePlaymode) {
            enabled = false;
            enteredPlaymodeViaStartRecordingButton = false;
        }
    }

    void OnEnable()
    {
        EditorApplication.playModeStateChanged += OnPlayModeStateChanged;

        recorder = null;
        videoPlayer = null;
        frameState = 0;
        frameRate = 60.0f;
        firstFrame = true;
        sourceCompleted = false;
        saved = false;
        prepared = false;
        preparedTime = float.PositiveInfinity;
        lastRealtimeSnapshot = float.NegativeInfinity;
        didMaximizeGameView = false;

        // Multiple simultaneous active TransformAnimRecorders should be fine in
        // principle, but could cause problems if one auto-exits playmode at the
        // end of recording before other isn't finished. Prevent it from happening
        // since it most likely is by accident.
        TransformAnimRecorder[] recorders = UnityEngine.Object.FindObjectsOfType<TransformAnimRecorder>();
        foreach (TransformAnimRecorder r in recorders) {
            if (r != this && r.isActiveAndEnabled) {
                Debug.LogError("Multiple TransformAnimRecorders enabled simultaneously.");
                Stop();
                return;
            }
        }


        /*
         * Perform input validation. These errors should not normally be
         * encountered because the Inspector will disable the 'Start Recording'
         * button if they would occur. But have these here just in case.
         */

        if (animator == null) {
            Debug.LogError("Animator not set on TransformAnimRecorder script. An Animator on the avatar is required.");
            Stop();
            return;
        }

        if (animator.GetComponentInChildren<SkinnedMeshRenderer>() == null) {
            Debug.LogError("No SkinnedMeshRenderer found on the avatar.");
            Stop();
            return;
        }

        if (armatureRoot != null) {
            if (!armatureRoot.transform.IsChildOf(animator.transform)) {
                Debug.LogError(
"'Armature Root' must be a child object of the Animator on TransformAnimRecorder. " +
"If your avatar's armature root is named 'Armature' or 'Root', this field can be " +
"left blank as it will automatically be filled in.");
                Stop();
                return;
            }
        }

        // Find an armature root object if one was not specified by the user
        if (armatureRoot == null) {
            Transform t = animator.transform.Find("Armature");
            if (t != null) {
                armatureRoot = t.gameObject;
            } else {
                t = animator.transform.Find("Root");
                if (t != null) {
                    armatureRoot = t.gameObject;
                } else {
                    Debug.LogWarning(
"No armature root could be found automatically. Using the object with the " +
"animator as armature root, which may result in extra undesired properties " +
"being recorded to the anim file.");
                    armatureRoot = animator.gameObject;
                }
            }
        }


        /*
         * Input validation specific for each source mode.
         *
         * Also, for ShaderMotionVideo, instantiate the MotionPlayerPrefab
         * object and verify ShaderMotion is installed.
         */

        if (sourceMode == SourceAnimationMode.AnimationClip) {
            if(sourceAnimClip == null) {
                Debug.LogError(
"Source Animation Clip not set on TransformAnimRecorder script. An Animation Clip " +
"is required for Source Mode set to 'Animation Clip'. It should probably be a " +
"Humanoid (Mecanim) animation.");
                Stop();
                return;
            }
        }
        else if (sourceMode == SourceAnimationMode.AnimationClip) {
            if(sourceAnimClip == null) {
                Debug.LogError(
"Source Animator Controller not set on TransformAnimRecorder script. An " +
"Animator Controller is required for Source Mode set to 'Animator Controller'. " +
"The animator controller needs an empty state on the Base layer named " +
"STOP_RECORDING. Recording will end as soon as the animator enters that state.");
                Stop();
                return;
            }
        }
        else if (sourceMode == SourceAnimationMode.ShaderMotionVideo) {
            string knownGUID = "f577b78ff2009fc4fb909dc80210fa36"; // Standard GUID of MotionPlayerPrefab
            string motionPlayerPrefabPath = AssetDatabase.GUIDToAssetPath(knownGUID);
            if (motionPlayerPrefabPath == "" || motionPlayerPrefabPath == null) {
                string[] lookupGUIDs = AssetDatabase.FindAssets("MotionPlayerPrefab", new[] {"Assets/BlenderImportUnityAnim"});
                if (lookupGUIDs.Length > 0) {
                    motionPlayerPrefabPath = AssetDatabase.GUIDToAssetPath(lookupGUIDs[0]);
                }
            }
            if (motionPlayerPrefabPath == "" || motionPlayerPrefabPath == null) {
                Debug.LogError("Missing MotionPlayerPrefab asset. Package may be installed incorrectly.");
                Stop();
                return;
            }

            GameObject motionPlayerPrefab = PrefabUtility.LoadPrefabContents(motionPlayerPrefabPath);
            if (motionPlayerPrefab == null) {
                Debug.LogError("Cannot load MotionPlayerPrefab asset. Package may be installed incorrectly.");
                Debug.Log("Asset path: " + motionPlayerPrefabPath);
                Stop();
                return;
            }

            motionPlayerInstance = Instantiate(motionPlayerPrefab);

            // Unload prefab to avoid memory leak
            PrefabUtility.UnloadPrefabContents(motionPlayerPrefab);

            motionPlayerInstance.transform.SetParent(animator.transform, false /*dont keep world position; move next to the avatar*/);
            motionPlayerInstance.transform.SetParent(transform, true /*keep world position*/);

            string installMsg1 =
"Make sure to install the ShaderMotion package from https://gitlab.com/lox9973/ShaderMotion/-/releases .";
            string installMsg2 =
"If ShaderMotion is installed, this error might be a bug, or be due to an " +
"update to ShaderMotion. Try uncommenting '#define SHADER_MOTION_INSTALLED' " +
"near the top of TransformAnimRecorder.cs.";

            // Set the 'animator' property on the MotionPlayer to match the animator set here.
            #if SHADER_MOTION_INSTALLED
                MotionPlayer motionPlayer = motionPlayerInstance.GetComponentInChildren<MotionPlayer>();
                if (motionPlayer == null) {
                    Debug.LogError($"No MotionPlayer component found. {installMsg1}");
                    Stop();
                    return;
                }
                motionPlayer.animator = animator;
            #else
                Type motionPlayerType = Type.GetType("ShaderMotion.MotionPlayer, ShaderMotion");
                MonoBehaviour motionPlayer = null;
                if (motionPlayerType != null) {
                    motionPlayer = motionPlayerInstance.GetComponentInChildren(motionPlayerType) as MonoBehaviour;
                }
                if (motionPlayer == null) {
                    Debug.LogError($"No MotionPlayer component found. {installMsg1} {installMsg2}");
                    Stop();
                    return;
                }
                FieldInfo animatorField = motionPlayerType.GetField("animator");
                if (animatorField == null) {
                    Debug.LogError($"MotionPlayer 'animator' property not found. {installMsg2}");
                    Stop();
                    return;
                }
                animatorField.SetValue((System.Object)motionPlayer, animator);
            #endif

            motionPlayer.enabled = true;

            // Find the video player component
            videoPlayer = motionPlayerInstance.GetComponentInChildren<VideoPlayer>();
            if (videoPlayer == null) {
                Debug.LogError("No VideoPlayer component found. Something is wrong with MotionPlayerPrefab.");
                Stop();
                return;
            }

            if(sourceVideo == null) {
                Debug.LogError(
"Source Video not set on TransformAnimRecorder script. A ShaderMotion video " +
"recording is required for Source Mode set to 'Shader Motion Video'.");
                Stop();
                return;
            }
        }


        /*
         * Prepare source and recorder.
         */

        // Remove any existing controller so that the Motion Player can control it.
        animator.runtimeAnimatorController = null;
        // Disable the animator so that it can be stepped forward manually with Update()
        animator.enabled = false;

        recorder = new GameObjectRecorder(animator.gameObject);
        recorder.BindComponentsOfType<Transform>(armatureRoot, true);

        if (sourceMode == SourceAnimationMode.AnimationClip) {
            frameRate = sourceAnimClip.frameRate;
            animator.runtimeAnimatorController = GenerateAnimationControllerWithClip(sourceAnimClip, footIK);
        }
        else if (sourceMode == SourceAnimationMode.AnimatorController) {
            realtimePlaybackAndRecording = true;
            frameRate = realtimeRecordingFPS;
            animator.runtimeAnimatorController = sourceAnimController;
        }
        else if (sourceMode == SourceAnimationMode.ShaderMotionVideo) {
            videoPlayer.clip = sourceVideo;
            videoPlayer.playOnAwake = false;
            videoPlayer.waitForFirstFrame = true;
            videoPlayer.isLooping = false;
            videoPlayer.enabled = true;
            videoPlayer.prepareCompleted += OnVideoPrepared;
            videoPlayer.loopPointReached += OnVideoEnded;
            videoPlayer.frameReady += OnFrameReady;
            videoPlayer.sendFrameReadyEvents = true;
            if (realtimePlaybackAndRecording) {
                videoPlayer.playbackSpeed = 1;
            }
            videoPlayer.Prepare();
        }

        // Maximizing the game view reduces the amount of other editor tasks
        // that need to be run each frame. Specifically, rendering the Scene
        // view or Animator view. This can dramatically speed up frame times
        // and therefore make animation playback faster.
        if (maximizeGameView) {
            Type gameViewType = Type.GetType("UnityEditor.GameView, UnityEditor");
            if (gameViewType != null) {
                EditorWindow[] gameViews = (EditorWindow[])Resources.FindObjectsOfTypeAll(gameViewType);
                if (gameViews.Length > 0) {
                    if (!gameViews[0].maximized) {
                        didMaximizeGameView = true;
                        gameViews[0].maximized = true;
                    }
                }
            }
        }
    }

    public void Stop()
    {
        // Disabling clears enteredPlaymodeViaStartRecordingButton, so save the value
        bool sv = enteredPlaymodeViaStartRecordingButton;
        // Invokes OnDisable
        enabled = false;
        if (sv) {
            EditorApplication.ExitPlaymode();
        }
    }

    void OnDisable()
    {
        SaveRecording();
        if (motionPlayerInstance != null) {
            Destroy(motionPlayerInstance);
        }
        enteredPlaymodeViaStartRecordingButton = false;

        // Unmaximize game view if we maximized it before
        if (didMaximizeGameView) {
            Type gameViewType = Type.GetType("UnityEditor.GameView, UnityEditor");
            if (gameViewType != null) {
                EditorWindow[] gameViews = (EditorWindow[])Resources.FindObjectsOfTypeAll(gameViewType);
                if (gameViews.Length > 0) {
                    gameViews[0].maximized = false;
                }
            }
        }
        didMaximizeGameView = false;

        EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
    }

    // Exiting play mode automatically calls OnDisable on everything, but for
    // some reason without explicitly doing 'enabled=false' to trigger OnDisable,
    // sometimes unity will exit playmode before OnDisable finishes, leading to
    // the recording not getting saved.
    void OnPlayModeStateChanged(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.ExitingPlayMode) {
            enabled = false;
        }
    }

    void SaveRecording()
    {
        if(saved || firstFrame) {
            return;
        }
        saved=true;

        string path = outputFileName.Trim();
        if (path == "") {
            path = "Recording.anim";
        }
        if (!path.EndsWith(".anim")) {
            path += ".anim";
        }
        path = "Assets/" + outputFileName;
        path = AssetDatabase.GenerateUniqueAssetPath(path);

        AnimationClip clip = new AnimationClip();
        AssetDatabase.CreateAsset(clip, path);

        #if UNITY_2019_4_OR_NEWER
            var filterOptions = new CurveFilterOptions() {
                keyframeReduction = true,
                positionError = keyframeReduction,
                rotationError = keyframeReduction,
                scaleError = keyframeReduction,
                floatError = keyframeReduction
            };
            recorder.SaveToClip(clip, (int)frameRate, filterOptions);
        #else
            recorder.SaveToClip(clip, (int)frameRate);
        #endif

        if (sourceMode == SourceAnimationMode.AnimationClip) {
            // Copy animation events from source anim
            AnimationUtility.SetAnimationEvents(clip, sourceAnimClip.events);

            // Copy settings (namely Loop Time and Cycle Offset) from source anim
            // Undocumented function Get/SetAnimationClipSettings ???
            // Only in "legacy" documentation https://docs.unity3d.com/462/Documentation/ScriptReference/AnimationClipSettings.html
            // NOT marked depcreated in https://github.com/Unity-Technologies/UnityCsReference/blob/e7d9de5f09767c3320b6dab51bc2c2dc90447786/Editor/Mono/Animation/AnimationUtility.bindings.cs#L412
            // Still works as of Unity 2019.4.31f1
            var settings = AnimationUtility.GetAnimationClipSettings(sourceAnimClip);
            AnimationUtility.SetAnimationClipSettings(clip, settings);
        }

        AssetDatabase.SaveAssets();
    }

    void LateUpdate()
    {
        if (sourceMode == SourceAnimationMode.AnimationClip ||
            sourceMode == SourceAnimationMode.AnimatorController) {
            Update_AnimatorController();
        }
        else if (sourceMode == SourceAnimationMode.ShaderMotionVideo) {
            Update_ShaderMotion();
        }
    }

    // In realtime recording mode, a snapshot is taken every playmode-frame,
    // which is likely much more frequent than the realtimeRecordingFPS or the
    // source animation framerate. For some reason GameObjectRecorder only has
    // you specify the desired framerate at the end of recording, in SaveToClip.
    // At that point, extra snapshots taken more frequently than the desired
    // FPS are effectively discarded. For a long recording and high playmode
    // FPS, those extra snapshots can consume a lot of RAM.
    // This variable is used to avoid the unnecessary RAM usage by only taking
    // snapshots at at-most 2x the rate of realtimeRecordingFPS. I'm not sure
    // what snapshot rate is best, but matching it exactly to realtimeRecordingFPS
    // seems like it could miss some data. I pick 2x in the spirit of the
    // Nyquist–Shannon sampling theorem, though I doubt it really makes a
    // difference here!
    void TakeSnapshotRealtime()
    {
        float desiredInterval = (1.0f/realtimeRecordingFPS) / 2.0f;

        // lastRealtimeSnapshot is initialized to -infinity, so this is
        // always true for the first snapshot. It also doesn't matter
        // that the dt arg will be +infinity for the first call, because
        // TakeSnapshot's docs say the first call ignores dt.
        if (Time.time >= lastRealtimeSnapshot + desiredInterval) {
            float dt = Time.time - lastRealtimeSnapshot;
            recorder.TakeSnapshot(dt);
            lastRealtimeSnapshot = Time.time;
        }
    }


    /*
     * AnimationClip and AnimatorController recording
     */

    AnimatorController GenerateAnimationControllerWithClip(AnimationClip clip, bool footIK)
    {
        AnimatorState s = new AnimatorState();
        s.name = clip.name;
        s.motion = clip;
        s.iKOnFeet = footIK;
        ChildAnimatorState cs = new ChildAnimatorState();
        cs.state = s;

        AnimatorState es = new AnimatorState();
        es.name = "STOP_RECORDING";
        ChildAnimatorState ces = new ChildAnimatorState();
        ces.state = es;

        AnimatorStateTransition t = new AnimatorStateTransition();
        t.name = "Stop Recording";
        t.hasExitTime = true;
        t.exitTime = 1;
        t.hasFixedDuration = true;
        t.duration = 0;
        t.destinationState = es;

        AnimatorStateTransition[] ta = new AnimatorStateTransition[1];
        ta[0] = t;

        s.transitions = ta;

        ChildAnimatorState[] csa = new ChildAnimatorState[2];
        csa[0] = cs;
        csa[1] = ces;

        AnimatorStateMachine sm = new AnimatorStateMachine();
        sm.states = csa;
        sm.defaultState = s;

        AnimatorControllerLayer l = new AnimatorControllerLayer();
        l.name = "Base Layer";
        l.stateMachine = sm;
        AnimatorControllerLayer[] la = new AnimatorControllerLayer[1];
        la[0] = l;

        AnimatorController c = new AnimatorController();
        c.layers = la;
        c.name = $"TransformAnimRecorder Generated Controller for {clip.name}";
        return c;
    }

    void Update_AnimatorController()
    {
        sourceCompleted = animator.GetCurrentAnimatorStateInfo(0).IsName("STOP_RECORDING");

        // Wait a few game-frames after OnEnable, when the animator controller
        // is set, to let the controller load. Then update by 0 time to make
        // it show the first animation-frame.
        if (!prepared) {
            if (frameState == 10) {
                animator.Update(0);
            }
            else if (frameState == 20) {
                prepared = true;
                preparedTime = Time.time;
                frameState = 0;
                return;
            }
            frameState++;
            return;
        }

        if (realtimePlaybackAndRecording) {
            // Wait until delay time is up, then start playback & recording
            if (Time.time < preparedTime + startDelaySeconds) {
                return;
            }

            // For reasons I don't understand, this snapshot needs to happen before
            // sourceCompleted is checked, unlike the not-realtime mode below.
            TakeSnapshotRealtime();

            if (sourceCompleted) {
                Stop();
                return;
            }

            if (firstFrame) {
                animator.enabled = true;
                firstFrame = false;
            }
        }
        else { // not realtime
            // Some delay between game-frames to make sure the animator has updated
            if (frameState == 0) {
                frameState++;
            }
            else {
                frameState = 0;
                firstFrame = false;
                if (sourceCompleted) {
                    Stop();
                    return;
                }
                else {
                    // Take snapshot only if not sourceCompleted so it doesn't
                    // capture after moving to the STOP_RECORDING state.
                    recorder.TakeSnapshot(1.0f/frameRate);
                    // Move animation clip forward by one animation-frame
                    animator.Update(1.0f/frameRate);
                }
            }
        }
    }


    /*
     * ShaderMotion recording
     */

    void OnVideoPrepared(VideoPlayer player)
    {
        if (realtimePlaybackAndRecording) {
            frameRate = realtimeRecordingFPS;
        } else {
            frameRate = player.frameRate;
        }

        prepared = true;
        preparedTime = Time.time;
        player.Play();
    }

    void OnFrameReady(VideoPlayer player, long frame)
    {
        if (realtimePlaybackAndRecording) {
            // Pause on first video-frame to wait until delay time is up
            player.Pause();
            // Video-frame ready events no longer needed
            videoPlayer.sendFrameReadyEvents = false;
        }
        else {
            // Pause every video-frame; Update will wait a few game-frames
            // to ensure the motion recording has been applied and then it
            // will take a snapshot and continue playing the video.
            frameState = 1;
            player.Pause();
        }
    }

    void Update_ShaderMotion()
    {
        if (realtimePlaybackAndRecording) {
            // Wait until delay time is up, then start playback & recording
            if (Time.time < preparedTime + startDelaySeconds) {
                return;
            }

            TakeSnapshotRealtime();
            if (sourceCompleted) {
                Stop();
                return;
            }

            if (firstFrame) {
                videoPlayer.Play();
                firstFrame = false;
            }
        }
        else { // not realtime
            if (frameState <= 0) {
                if (sourceCompleted) {
                    Stop();
                    return;
                }
            }
            // Delay a few game-frames to make sure MotionPlayer has updated the armature
            // Wait extra long on the first game-frame, might still be loading in
            else if ((firstFrame && frameState <= 90) || (!firstFrame && frameState <= 3)) {
                frameState++;
            }
            else {
                frameState = 0;
                recorder.TakeSnapshot(1.0f/frameRate);
                firstFrame = false;
                if (sourceCompleted) {
                    Stop();
                    return;
                }
                else {
                    // Play to move to next video-frame
                    videoPlayer.Play();
                }
            }
        }
    }

    void OnVideoEnded(VideoPlayer player)
    {
        sourceCompleted = true;
        player.Stop();
    }
}

}
#endif
