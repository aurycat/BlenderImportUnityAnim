# Blender importer for Unity .anim files
# ------------------------------------------------------------------------------
#
# Imports .anim files containing transform animation data. The armature
# must be identical in Unity and Blender for the import to work correctly.
# Ideally, the armature being animated in Unity comes directly from an
# .fbx created by Blender.
#
# ------------------------------------------------------------------------------
#
# Most armature animations are not stored as transform animations, they
# are Humanoid animations which will not work with this script. Therefore
# they need to be converted. You can use the attached Animation Conversion
# script to perform this conversion.
#
# Please consult the README for instructions on how to convert both Humanoid
# animations and ShaderMotion recordings into non-humanoid/transform animations
# which are compatible with this importer.
#
# ------------------------------------------------------------------------------
#
# AUTHOR: aurycat
# LICENSE: MIT
# HISTORY:
#  1.0 (2023-07-04)

bl_info = {
    "name": "Import Unity Anim",
    "author": "aurycat",
    "version": (0, 1, 0),
    "blender": (2, 80, 0),
    "location": "File > Import > Unity Anim (.anim)",
    "description": "Imports Unity anim files which animate armature bones onto an existing armature",
    "wiki_url": "https://gitlab.com/aurycat/BlenderImportUnityAnim",
    "category": "Import-Export",
}

import pprint
import os
import bpy
import sys
import time
import math
import mathutils
import numbers
import bpy_extras

class FailedException(Exception):
    pass


def ReadUnityAssetFile(path):
    """
    Reads a Unity asset file into a Python array/dictionary structure.
    Supports a subset of UnityYAML, Unity's textual asset representation,
    which is in turn a subset of YAML, plus a few non-conforming things.
    References:
        https://docs.unity3d.com/Manual/UnityYAML.html
        https://stackoverflow.com/questions/21473076/pyyaml-and-unusual-tags/27117480#27117480

    This parser does not handle: quoted scalars, nested flow sequences,
    ie. [ [...] ], or nested flow mappings, ie. { {...} } . Unity uses
    quoted scalars for storing multiline strings, e.g. the text field on
    a UI Text element. I have never seen Unity use nested flow sequences/
    mappings. If this parser encounteres either of those things, it will
    output the value None for that property. This parser also only accepts
    2-space indents, which appears to be all that Unity uses.

    I chose to write a parser instead of using PyYAML because PyYAML is
    incredibly slow for the needs of this program. Importing a 35 MB .anim
    file took PyYAML over 2 minutes but only took this function a few seconds.

    Returns an array, each element is a dictionary representing one asset
    in the file. Each asset will always have the TYPENAME, TYPEID, and FILEID
    properties, which represent these parts from the asset file:
        --- !u!TYPEID &FILEID
        TYPENAME:
    The rest of the properties in the asset dictionary are determined by
    the contents of the asset in the file.
    """
    global report
    assets = []
    stack = []
    lineno = 0
    prop_name = None
    current_obj = None
    multiline_val = None

    def fail(msg):
        msg = "Error reading asset file [line {line}]: {msg}".format(line=lineno,msg=msg)
        report({'ERROR'}, msg)
        print(msg)
        raise FailedException()
    def warn(msg):
        msg = "Warning reading asset file [line {line}]: {msg}".format(line=lineno,msg=msg)
        report({'WARNING'}, msg)
        print(msg)

    def try_basic_types(v):
        if v == "true": return True
        elif v == "false": return False
        try:
            return int(v)
        except ValueError:
            try:
                return float(v)
            except ValueError:
                return v

    def parse_value(v):
        if v == "":
            return ""
        elif "\"" in v or "'" in v:
            warn("The property '{prop}' contains a string, which is not supported. This property will be skipped.")
            return None
        elif v[0] == '[':
            if v == "[" or v[-1] != ']':
                fail("Unterminated [ ] block or trailing characters")
            v = v[1:-1]
            if ("[" in v) or ("{" in v) or ("[" in v) or ("{" in v):
                warn("The property '{prop}' contains nested {{ }} or [ ], which are not supported. This property will be skipped.")
                return None
            if v == "":
                return []
            return [try_basic_types(e) for e in v.split(",")]
        elif v[0] == '{':
            if v == "{" or v[-1] != '}':
                fail("Unterminated { } block or trailing characters")
            v = v[1:-1]
            if ("[" in v) or ("{" in v) or ("[" in v) or ("{" in v):
                warn("The property '{prop}' contains nested {{ }} or [ ], which are not supported. This property will be skipped.")
                return None
            obj = {}
            if v == "":
                return obj
            for kv in v.split(","):
                kvs = kv.split(":")
                if len(kvs) != 2:
                    fail("Incorrect number of ':' in key/val pair")
                obj[kvs[0].strip()] = try_basic_types(kvs[1].strip())
            return obj
        else:
            return try_basic_types(v)

    with open(path, 'r') as file:
        for line in file:
            lineno += 1
            is_array_element_start = False
            obj_name = None
            sline = line.lstrip()
            # skip empty lines and initial tags
            if sline == "" or line[0] == '%':
                continue
            # read asset start marker
            if line.startswith("---"):
                asset_typeid = None
                asset_fileid = None
                multiline_val = None
                current_obj = None
                prop_name = None
                parts = line.split(" ")
                if len(parts) >= 3:
                    if parts[1].startswith("!u!"):
                        try: asset_typeid = int(parts[1][3:])
                        except ValueError: pass
                    if parts[2].startswith("&"):
                        try: asset_fileid = int(parts[2][1:])
                        except ValueError: pass
                if asset_typeid == None or asset_fileid == None:
                    fail("Invalid asset marker")
                assets.append({'TYPEID':asset_typeid, 'FILEID':asset_fileid})
                continue
            if len(assets) == 0:
                fail("Start of data before first --- marker")
            # compute indent level
            # unity appears to always use 1 indent = 2 spaces
            indent_lv = len(line) - len(sline)
            if indent_lv % 2 == 1:
                fail("Invalid indent level")
            indent_lv = indent_lv // 2
            is_array_element_start = sline.startswith("- ")
            # handle multiline vals by concating every line until the
            # indent level returns to where it was when the val started
            if multiline_val != None:
                if indent_lv > len(stack):
                    if multiline_val == "":
                        # the previous line was not a property:value
                        # pair, it was the name of a child object
                        obj_name = prop_name
                    else:
                        multiline_val += " " + sline.strip()
                        continue
                elif is_array_element_start and multiline_val == "":
                    obj_name = prop_name
                else:
                    # current_obj and prop_name are from the previous
                    # loop iteration that started this property
                    lineno -= 1
                    current_obj[prop_name] = parse_value(multiline_val)
                    lineno += 1
                multiline_val = None
            # check for array element marker
            # the "- " is considered part of the indent
            if is_array_element_start:
                if indent_lv == 0:
                    fail("Invalid array at toplevel")
                indent_lv += 1
                sline = sline[2:]
                tmp = sline.lstrip()
                if len(sline) != len(tmp):
                    fail("Expecting property name after array marker")
                sline = tmp
            # read property name, which is terminated by a colon
            colon = sline.find(":")
            if colon == -1:
                fail("Expecting colon after property name")
            prop_name = sline[:colon].rstrip()
            if prop_name == "" or prop_name.startswith("-"):
                fail("Invalid property name")
            # everything after the colon is the property value
            sline = sline[colon+1:].strip()
            # level 0 indent should only be at the start of a new asset
            # and specifices the type name of the asset
            if indent_lv == 0:
                if sline != "":
                    fail("Unexpected data after colon on asset name line")
                current_asset = assets[-1]
                if 'TYPENAME' in current_asset:
                    fail("Starting new asset without another --- marker")
                current_asset['TYPENAME'] = prop_name
                stack = []
                multiline_val = None
                current_obj = None
                prop_name = None
                continue
            # using indent level, adjust the object nesting stack and set
            # current_obj to the object that this property belongs to
            if indent_lv > len(stack)+1: # indent increased by more than 1
                fail("Incorrect indent level")
            elif indent_lv == len(stack)+1: # indent increased by 1
                if indent_lv == 1:
                    current_obj = assets[-1]
                    stack.append(current_obj)
                elif obj_name == None:
                    fail("Unexpected start of new object")
                else:
                    current_obj = stack[-1]
                    if isinstance(current_obj, list):
                        current_obj = current_obj[-1]
                    new_obj = {}
                    new_objarr = [new_obj] if is_array_element_start else new_obj
                    current_obj[obj_name] = new_objarr
                    stack.append(new_objarr)
                    current_obj = new_obj
            elif indent_lv <= len(stack): # indent unchanged or decreased
                if indent_lv < len(stack): # indent decreased
                    stack = stack[:indent_lv]
                if is_array_element_start:
                    current_obj = stack[-1]
                    if isinstance(current_obj, list):
                        new_obj = {}
                        current_obj.append(new_obj)
                        current_obj = new_obj
                    else:
                        fail("Invalid position for array element marker")
                else:
                    current_obj = stack[-1]
                    if isinstance(current_obj, list):
                        current_obj = current_obj[-1]
            if current_obj == None:
                fail("ASSERT FAIL current_obj == None")
            # current_obj[prop_name] is now the thing to assign this line's
            # value into. store the rest of the line -- the value -- into a
            # temp and process it later once any line continuations are read
            multiline_val = sline

        # EOF reached. check for any remaining data to write
        if multiline_val != None:
            current_obj[prop_name] = parse_value(multiline_val)

    return assets


def ParseCurve(curve, sample_rate, val_type='?'):
    """
    curve: A 'curve' object from an AnimationClip asset.
    sample_rate: The sample rate for converting seconds into frames.
    val_type: 'Q' to expect Quaternion values, 'V' to expect Vector3
              values, 'F' to expect float values, or '?' to choose
              based on the first value found.
    Returns { path: string split by '/',
              keys: [ {frame:int, value:<float or mathutils.Vector or mathutils.Quaternion>},
                      ...
                    ]
            }
    or ({},errmsg) if unreadable
    """

    result = {}

    if not isinstance(curve, dict): return result, "Incorrect structure", None

    path = curve.get('path')
    if path is None: return result, "Missing path property", None
    result['path'] = str(path).split('/')

    _curve = curve.get('curve')
    if _curve is None: return result, "Missing curve property", None
    if not isinstance(_curve, dict): return result, "Curve property is not an object", None

    m_Curve = _curve.get('m_Curve')
    if not isinstance(m_Curve, list): return result, "m_Curve property is not a list", None

    keys = []

    i = 0
    for srckey in m_Curve:
        i = i + 1
        if not isinstance(srckey, dict):
            return result, "Keyframe {i} has incorrect structure".format(i=i), None

        time = srckey.get('time')
        if time is None:
            return result, "Keyframe {i} is missing time property".format(i=i), None
        if not isinstance(time, numbers.Real) or time < 0:
            return result, "Keyframe {i} has invalid time".format(i=i), None

        v = srckey.get('value')
        if v is None:
            return result, "Keyframe {i} is missing value property".format(i=i), None
        if isinstance(v, numbers.Real):
            if val_type == '?':
                val_type = 'F'
            elif val_type != 'F':
                return result, "Expected float value for Keyframe {i}".format(i=i), None
            v = float(v)
        elif isinstance(v, dict):
            if 'x' in v and 'y' in v and 'z' in v:
                if 'w' in v:
                    if val_type == '?':
                        val_type = 'Q'
                    elif val_type != 'Q':
                        return result, "Expected Quaternion value for Keyframe {i}".format(i=i), None
                    v = mathutils.Quaternion((v['w'], v['x'], v['y'], v['z']))
                else:
                    if val_type == '?':
                        val_type = 'V'
                    elif val_type != 'V':
                        return result, "Expected Vector value for Keyframe {i}".format(i=i), None
                    v = mathutils.Vector((v['x'], v['y'], v['z']))
            else:
                return result, "Keyframe {i} has unreadable value property".format(i=i), None
        else:
            return result, "Keyframe {i} has unreadable value property".format(i=i), None

        frame = round(time*sample_rate)

        keys.append({'frame': frame, 'value':v})

    result['keys'] = keys
    return result, None, val_type


def UnityToBlenderQuaternion(q):
    """
    Converts a mathutils.Quaternion value from Unity's coordinate system
    to Blender's.
    """
    return mathutils.Quaternion(( # unity => blender
        q.w,                      #    W  =>  W
        q.x,                      #    X  =>  X
        -q.y,                     #   -Y  =>  Y
        -q.z))                    #   -Z  =>  Z


def TryReadCurve(curve, curve_name, arm, i, applied_bones, sample_rate, val_type):
    global report
    global missing_bones

    pcurve, errmsg, val_typ = ParseCurve(curve, sample_rate, val_type)
    if errmsg != None:
        # Try to avoid spamming errors
        path = pcurve.get('path')
        bone_name = path[-1] if path != None else None
        if bone_name != None and bone_name in applied_bones:
            return None
        report({'WARNING'}, "Warning: {curve_name} curve {i} for bone '{name}' is unreadable and will be ignored (reason: {m})".
            format(curve_name=curve_name, i=i, name=bone_name or "???", m=errmsg))
        return None

    path = pcurve['path']

    # Ignore curves on the armature itself
    # These might be nonzero due to Unity's import axis conversions
    if len(path) == 1 and (path[0] == arm.name or
                           path[0].lower() == "armature"):
        return None

    bone_name = path[-1]
    if bone_name in applied_bones or bone_name in missing_bones:
        return None
    applied_bones.append(bone_name)

    if not bone_name in arm.pose.bones:
        missing_bones.append(bone_name)
        report({'WARNING'}, "Warning: Bone '{b}' not found in target armature and will be skipped".format(b=bone_name))
        return None

    return (pcurve, bone_name)


def ClearKeyframesForProperty(ob, data_path):
    """
    Removes all the keyframes in the active NLA track (the keyframes seen
    in the Timeline window) for a property on an object. For example,
        ob=bpy.data.objects['Armature']  and  data_path='pose.bones["Hips"].location'
    will remove all 'location' keyframes for the Hips pose bone on Armature.
    """
    if ob.animation_data == None:
        return
    action = ob.animation_data.action
    if action == None:
        return
    for fc in action.fcurves:
        if fc.data_path == data_path:
            action.fcurves.remove(fc)

def ClearKeyframesForPropertyOnPoseBone(armature, bone_name, property):
    data_path = 'pose.bones["{bone_name}"].{property}'.format(bone_name=bone_name, property=property)
    ClearKeyframesForProperty(armature, data_path)


def ApplyRotationCurvesToArmature(curves, arm, applied_bones, sample_rate, clear_existing_keyframes):
    """
    Applies an array of curves, e.g. the `m_RotationCurves` property from
    an AnimationClip, to an existing armature. If a bone is defined in the
    anim but not present on the armature, a warning will be emitted in
    the console.

    Currently does not support curve weights/tangents/etc, only considers
    the keyframe value.

    arm: The Armature object to import onto.
    applied_bones (in/out): An array of bone names that have been applied.
                            If a bone is in this array, it will be skipped.
                            This lets importing multiple curve sets from the
                            AnimationClip with a "preference order".
    sample_rate: The sample rate for converting seconds into frames.
    curves: The array of 'curve' objects from an AnimationClip.
    """
    i = 0
    for curve in curves:
        i = i + 1
        data = TryReadCurve(curve, "Rotation", arm, i, applied_bones, sample_rate, 'Q')
        if data == None:
            continue

        pcurve = data[0]
        bone_name = data[1]
        pbone = arm.pose.bones[bone_name]
        pbone.rotation_mode = 'QUATERNION'

        if clear_existing_keyframes:
           ClearKeyframesForPropertyOnPoseBone(arm, bone_name, "rotation_quaternion")

        # Get the t-pose basis rotation, relative to parent bone
        basis_rotation_inverted = pbone.bone.matrix.to_quaternion().inverted()

        for key in pcurve['keys']:
            # TODO: Will Quaternion axis conversion always be the same,
            # or does it depend on how the model is imported in Unity?
            rot = UnityToBlenderQuaternion(key['value'])

            # Apply inverted basis to the rotation from unity, which
            # gives us the pose bone rotation relative to the basis
            rot.rotate(basis_rotation_inverted)

            # Apply pose bone rotation
            pbone.rotation_quaternion = rot
            pbone.keyframe_insert(data_path="rotation_quaternion", frame=key['frame'])


def ApplyEulerCurvesToArmature(curves, arm, applied_bones, sample_rate, clear_existing_keyframes):
    """
    Applies euler rotation curves from the m_EulerCurves anim property
    """
    i = 0
    for curve in curves:
        i = i + 1
        data = TryReadCurve(curve, "Euler rotation", arm, i, applied_bones, sample_rate, 'V')
        if data == None:
            continue

        pcurve = data[0]
        bone_name = data[1]
        pbone = arm.pose.bones[bone_name]
        # Unity performs Euler rotations in ZXY order
        # TODO: Apparenly Unity performs Euler rotations around the *global*
        # ZXY axis, not local. I don't fully understand what that means, and
        # I'm not sure how that affects this importing. This SO page
        #   https://gamedev.stackexchange.com/a/138366
        # describes the rotations around the "local" axis, saying ZXY order
        # around the global axes is the same as YXZ order around local except
        # X is done around the "resulting" local X axis, ditto for Z. I don't
        # know how to implement that here, though. Just using ZXY *seems* to
        # work, though....
        pbone.rotation_mode = 'ZXY'

        if clear_existing_keyframes:
           ClearKeyframesForPropertyOnPoseBone(arm, bone_name, "rotation_euler")

        # Get the t-pose basis rotation, relative to parent bone
        basis_rotation_inverted = pbone.bone.matrix.to_quaternion().inverted()

        for key in pcurve['keys']:
            rot_deg = key['value']
            rot = mathutils.Euler((
                math.radians(rot_deg.x),
                math.radians(-rot_deg.y),
                math.radians(-rot_deg.z)),
                pbone.rotation_mode)

            # Apply inverted basis to the rotation from unity, which
            # gives us the pose bone rotation relative to the basis
            rot.rotate(basis_rotation_inverted)

            # Apply pose bone rotation
            pbone.rotation_euler = rot
            pbone.keyframe_insert(data_path="rotation_euler", frame=key['frame'])


def ApplyPositionCurvesToArmature(curves, arm, applied_bones, sample_rate, clear_existing_keyframes, armature_scale):
    i = 0
    for curve in curves:
        i = i + 1
        data = TryReadCurve(curve, "Position", arm, i, applied_bones, sample_rate, 'V')
        if data == None:
            continue

        pcurve = data[0]
        bone_name = data[1]
        pbone = arm.pose.bones[bone_name]

        if clear_existing_keyframes:
           ClearKeyframesForPropertyOnPoseBone(arm, bone_name, "location")

        parent_matrix_local = pbone.parent.bone.matrix_local if pbone.parent != None else mathutils.Matrix()
        matrix_local_inverted = pbone.bone.matrix_local.inverted()

        for key in pcurve['keys']:
            # This is the bone position as stored in Unity, which is
            # relative to the parent bone's head, in the coordinate
            # system of the parent.
            #
            # Note this is NOT stored the same way as the 'head' property
            # of a Bone object in Blender, which is relative to the parent
            # bone's *tail*, in the coordinate system of the parent.
            #
            # TODO: Aside from inverting the X, the coordinate system
            # should match on models imported from blender since Unity
            # applies the 90 degree rotation to the outer model/armature.
            # But what about models imported from other places? The axis
            # conversion here probably needs to be configurable on import.
            pos = key['value']
            pos = mathutils.Vector((
                -pos.x,
                pos.y,
                pos.z))

            # It is common for models in Unity to have a scale factor of 100
            # when imported from Blender, and all the armature bone positions
            # will be 1/100th their real values. That needs to be un-done first.
            pos *= armature_scale

            # pos is in the coordinate system of the parent basis bone
            # (not the pose bone!), so multiply by the parent's matrix_local
            # to convert from parent-head-local space to armature-local space.
            #
            # In other words, armature_local_pos is the position of the bone
            # that you'd see in Edit mode. In other other words, it is in the
            # same coordinate system as a Bone's head_local property.
            armature_local_pos = parent_matrix_local @ pos

            # Now convert from armature-local pos to the coordinate system
            # of the bone we're actually posing here. This gives us the
            # offset of the animated position from the basis position, which
            # is the pose-bone position.
            pose_pos = matrix_local_inverted @ armature_local_pos

            # Apply pose bone position
            pbone.location = pose_pos
            pbone.keyframe_insert(data_path="location", frame=key['frame'])


def ApplyScaleCurvesToArmature(curves, arm, applied_bones, sample_rate, clear_existing_keyframes):
    i = 0
    for curve in curves:
        i = i + 1
        data = TryReadCurve(curve, "Scale", arm, i, applied_bones, sample_rate, 'V')
        if data == None:
            continue

        pcurve = data[0]
        bone_name = data[1]
        pbone = arm.pose.bones[bone_name]

        if clear_existing_keyframes:
            ClearKeyframesForPropertyOnPoseBone(arm, bone_name, "scale")

        for key in pcurve['keys']:
            scale = key['value']
            # TODO: Axis conversion?
            pbone.scale = scale
            pbone.keyframe_insert(data_path="scale", frame=key['frame'])


def ImportAnimToArmature(anim_asset_file, arm, clear_existing_keyframes=True, armature_scale=100):
    """
    Reads in a Unity .anim file and applies the animation curves
    to an existing armature.
    """
    global report

    report({'INFO'}, "Reading .anim file '{name}'".format(name=anim_asset_file))

    assets = ReadUnityAssetFile(anim_asset_file)
    anims = [a for a in assets if a.get('TYPENAME') == 'AnimationClip']

    if len(anims) == 0:
        report({'ERROR'}, "Error: Asset file does not contain any AnimationClips.")
        raise FailedException()
    if len(anims) > 1:
        report({'WARNING'}, "Warning: Asset file contains multiple AnimationClips. Using the first one.")
    anim = anims[0]

    report({'INFO'}, "Importing AnimationClip '{anim}' onto armature '{arm}'"
        .format(anim=anim.get("m_Name", "???"), arm=arm.name))

    if anim.get('m_Compressed', 0) == 1:
        # TODO: I haven't seen an .anim file which uses compressed curves, so I'm
        # not sure how they work or what they're for.
        report({'ERROR'}, "Error: This anim uses compressed curves, which is not supported by this importer.")
        raise FailedException()

    m_SampleRate = anim.get('m_SampleRate', 60)

    # Explanation for all these different Curve objects:
    #
    # .anim files can store the same transform data in multiple different ways,
    # and not all ways will be present in all files. Notably, the "EditorCurves"
    # data contain the raw keyframe data shown in Unity's Timeline editor.
    # These store X, Y, and Z data in separate curves which allows them to be
    # controlled separately and have separate keyframes. Meanwhile, the non-editor
    # curve data is Unity's generated "combined" curves, which flatten the X, Y
    # and Z editor curves into a single curve. This loses the original keyframe
    # data (if the anim was handmade in the Timeline editor), but is easier to
    # work with. When Unity builds a project for release, it throws out the editor
    # curves.
    #
    # RotationCurves are rotations stored in quaternions, and usually only appear
    # when the anim file was generated by code. EulerCurves are rotations stored
    # in euler angles, and are generated when the anim is created from the editor.
    # PositionCurves and ScaleCurves are present in both handmade & generated anims.
    #
    # Although the Editor curves are potentially more accurate to the original anim
    # data, I'm only importing the non-editor curves right now because it's easier
    # and probably not a big deal in most cases. My main concern is that some anim
    # files might *only* have editor curves, but I haven't seen this happen naturally.
    #
    # FloatCurves are used to store other non-transform curves, so they're not of
    # interest here.
    m_RotationCurves    = anim.get('m_RotationCurves', [])     # Quaternion rotations (script-generated anims)
    m_EulerCurves       = anim.get('m_EulerCurves', [])        # Combined Euler rotations
    m_PositionCurves    = anim.get('m_PositionCurves', [])     # Combined positions
    m_ScaleCurves       = anim.get('m_ScaleCurves', [])        # Combined scales
    m_EditorCurves      = anim.get('m_EditorCurves', [])       # Original/separate position & scale (& possibly more?)
    m_EulerEditorCurves = anim.get('m_EulerEditorCurves', [])  # Original/separate euler rotation
    m_FloatCurves       = anim.get('m_FloatCurves', [])        # Other non-transform curves

    no_importable_curves = len(m_RotationCurves)==0 and len(m_EulerCurves)==0 and \
                           len(m_PositionCurves)==0 and len(m_ScaleCurves)==0
    has_editor_curves    = len(m_EditorCurves)>0 or len(m_EulerEditorCurves)>0
    has_float_curves     = len(m_FloatCurves)>0

    if no_importable_curves:
        if not has_editor_curves and not has_float_curves:
            report({'ERROR'}, "Error: This anim is empty.")
        elif has_float_curves:
            report({'ERROR'},
"""\
Error: This anim contains no readable transform curves. It might be
a Mecanim animation. Mecanim humanoid animations must be converted
to bone transform animations in Unity prior to using this importer.""")
        else:
            report({'ERROR'},
"""\
Error: This anim only contains so-called 'editor curves'. Editor curves
are used by Unity's Animation Timeline editor and are not supported by
this importer. Try opening this anim in Unity and re-saving, which should
cause Unity to generate 'compiled' curves. Then retry importing.""")
        raise FailedException()

    if has_float_curves:
        report({'WARNING'},
"""\
Warning: This anim contains extra curves (possibly Mecanim curves)
that are not supported by this importer and have been skipped. Mecanim
humanoid animations must be converted to bone transform animations in
Unity prior to using this importer.""")

    if (len(m_EditorCurves)>0 and len(m_PositionCurves)==0 and len(m_ScaleCurves)==0) or \
         (len(m_EulerEditorCurves)>0 and len(m_RotationCurves)==0 and len(m_EulerCurves)==0):
        report({'WARNING'},
"""\
Warning: This anim contains so-called 'editor curves', but are missing
some associated 'compiled' curves. Editor curves are not supported by
this importer, so the animation might not be fully imported. If so, try
opening and re-saving this animation in Unity, which should cause Unity
to generate the compiled curves. Then, retry importing.""")


    # These 'applied' arrays are used so that multiple curve sets can be imported
    # and the first curve found for a bone "wins", and any others are ignored.
    # Thus the order of "Apply" calls below sets the preference order for which
    # curves are applied if two different curves have data for the same bone.
    # TODO: If Editor curves are supported in the future, they should probably get
    # highest preference since they represent the original keyframe data.
    #
    # Other than that, the order of "Apply"s across rotation, position, and scale
    # curves shouldn't matter.
    applied_position_bones = []
    applied_rotation_bones = []
    applied_scale_bones = []

    ApplyRotationCurvesToArmature(m_RotationCurves, arm, applied_rotation_bones, m_SampleRate, clear_existing_keyframes)
    ApplyEulerCurvesToArmature(m_EulerCurves, arm, applied_rotation_bones, m_SampleRate, clear_existing_keyframes)
    ApplyPositionCurvesToArmature(m_PositionCurves, arm, applied_position_bones, m_SampleRate, clear_existing_keyframes, armature_scale)
    ApplyScaleCurvesToArmature(m_ScaleCurves, arm, applied_scale_bones, m_SampleRate, clear_existing_keyframes)


def object_is_armature(o):
    return (o.data is not None) and (o.pose is not None) and isinstance(o.data, bpy.types.Armature)

def list_armatures():
    return [ o.name for o in bpy.data.objects if object_is_armature(o) ]

def armature_items_cb(self, context):
    arr = [('##NONE##', "-Select an armature-", "")]
    arr.extend([ (name, name, "") for name in list_armatures() ])
    return arr


class ImportUnityAnim(bpy.types.Operator, bpy_extras.io_utils.ImportHelper):
    """Import Unity Anim File"""
    bl_idname = "import_unity_anim.import_anim"
    bl_label = "Import Anim"
    bl_options = {'REGISTER', 'UNDO', 'PRESET'}

    any_warnings = False

    # Dear future self: this syntax is Python annotations.
    # Blender uses them for property trickery.
    # https://blender.stackexchange.com/questions/229707/blenders-property-definition-is-this-standard-python-type-annotation
    # https://docs.blender.org/api/current/bpy.props.html
    filter_glob: bpy.props.StringProperty(
        default='*.anim',
        options={'HIDDEN'}
    )
    filename_ext = '.anim'

    armature: bpy.props.EnumProperty(
        name='Target Armature',
        description=
"""Select the armature to import the animation onto. This armature must be \
identical to the armature the animation was created for in Unity. Ideally \
it's either the original armature that was imported into Unity, or it's the \
same FBX as used in Unity""",
        items = armature_items_cb,
        default=0,
    )

    clear_keyframes: bpy.props.BoolProperty(
        name="Clear Existing Keyframes",
        description=
"""Remove all existing keyframes on properties that are affected by the \
imported animation""",
        default=True)

    unity_import_scale: bpy.props.FloatProperty(
        name="Scale Factor",
        description=
"""Match this value to the 'Scale Factor' model import setting of your \
avatar in Unity. It is 1 by default.
(Select your model in the Assets and then look at the Inspector, there will \
be a setting labeled 'Scale Factor')""",
        default=1)

    unity_convert_units: bpy.props.BoolProperty(
        name="Convert Units",
        description=
"""Match this value to the 'Convert Units' model import setting of your \
avatar in Unity. It is on by default for FBX models.
(Select your model in the Assets and then look at the Inspector, there will \
be a setting labeled 'Convert Units')""",
        default=True)

    armature_scale: bpy.props.FloatProperty(
        name="Armature Scale",
        description=
"""Match this value to the scale factor on the toplevel armature GameObject \
in your avatar's hierarchy in Unity. It is usually 100.
(Select your avatar in the scene, and in the hierarchy for it, there should \
be a child object probably called 'Armature' or 'Root'. Copy the X/Y/Z scale \
-- all three should be equal! -- shown in the Transform for that object)""",
        default=100)

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False

        layout.separator(factor=0.2)
        box = layout.box()
        box.scale_y = 0.5
        box.label(text="Mouseover each setting below")
        box.label(text="to see usage info.")
        layout.separator(factor=2)

        layout.prop(self, 'armature')
        layout.prop(self, 'clear_keyframes')

        layout.separator(factor=3)
        box = layout.box()
        box.scale_y = 0.5
        box.label(text="IMPORTANT: Match the following")
        box.label(text="properties to their corresponding")
        box.label(text="values in Unity. Hover your mouse")
        box.label(text="over each option for more info.")

        layout.prop(self, 'unity_import_scale')
        layout.prop(self, 'unity_convert_units')
        layout.prop(self, 'armature_scale')

    def report_and_print(self, type, message):
        self.report(type, message)
        print(message)
        if 'WARNING' in type:
            self.any_warnings = True

    def execute(self, context):
        global report
        global missing_bones

        report = self.report_and_print

        if self.armature == '##NONE##' or self.armature not in bpy.data.objects:
            report({'ERROR'}, "Error: No armature chosen for anim import. Choose an armature in the 'Target Armature' import option.")
            return {'FINISHED'}

        final_scale = \
            1.0/self.unity_import_scale * \
            self.armature_scale * \
            100*(0.01 if self.unity_convert_units else 1.0)

        try:
            self.any_warnings = False
            missing_bones = []
            ImportAnimToArmature(
                anim_asset_file          = self.filepath,
                arm                      = bpy.data.objects[self.armature],
                clear_existing_keyframes = self.clear_keyframes,
                armature_scale           = final_scale
            )
            if not self.any_warnings:
                report({'INFO'}, "Import completed successfully")
            else:
                report({'WARNING'}, "Import completed with warnings")
        except FailedException:
            pass

        return {'FINISHED'}

    def invoke(self, context, event):
        # Pick default value for armature dropdown
        armature_names = list_armatures()
        if len(armature_names) == 1:
            # If there's only one armature, select that one
            self.armature = armature_names[0]
        elif bpy.context.active_object is not None and bpy.context.active_object.name in armature_names:
            # Otherwise, if the user has selected an armature already, select that one
            self.armature = bpy.context.active_object.name

        return super().invoke(context, event)


def menu_func(self, context):
    self.layout.operator(ImportUnityAnim.bl_idname, text="Unity Armature Anim (.anim)", icon='OUTLINER_OB_ARMATURE')

def register():
    bpy.utils.register_class(ImportUnityAnim)
    bpy.types.TOPBAR_MT_file_import.append(menu_func)

def unregister():
    bpy.types.TOPBAR_MT_file_import.remove(menu_func)
    bpy.utils.unregister_class(ImportUnityAnim)


# For running from the built-in editor
# Note: Can use Blender > System > Reload Scripts to unload the class
# regisered here and any menus created
if __name__ == "__main__":
    bpy.utils.register_class(ImportUnityAnim)
    bpy.ops.import_unity_anim.import_anim('INVOKE_DEFAULT')
